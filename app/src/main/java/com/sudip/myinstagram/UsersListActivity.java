package com.sudip.myinstagram;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class UsersListActivity extends AppCompatActivity {
    TextView welcome;
    ListView userList;

    public void logOut(View view) {
        ParseUser.logOut();
        finish();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);
        ParseUser user = ParseUser.getCurrentUser();
        if (user == null) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            welcome = (TextView) findViewById(R.id.welcome);
            welcome.setText("Hi! " + user.getUsername());
            userList = (ListView) findViewById(R.id.userList);
            ParseQuery<ParseUser> query = ParseUser.getQuery();
            query.whereNotEqualTo("username",user.getUsername());
            query.addAscendingOrder("username");
            query.findInBackground(new FindCallback<ParseUser>() {
                @Override
                public void done(List<ParseUser> objects, ParseException e) {
                    ArrayList<String> usernames = new ArrayList<>();
                    for (ParseUser object : objects) {
                        usernames.add(object.getUsername());
                    }
                    ArrayAdapter<String> userListAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1,usernames);
                    userList.setAdapter(userListAdapter);
                }
            });
        }
    }
}