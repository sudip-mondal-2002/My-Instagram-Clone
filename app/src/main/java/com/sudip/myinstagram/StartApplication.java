package com.sudip.myinstagram;


import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;

public class StartApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Enable local datastore
        Parse.enableLocalDatastore(this);

        // Add your initialization code here
        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId(BuildConfig.APP_ID)
                .clientKey(BuildConfig.CLIENT_KEY)
                .server(BuildConfig.SERVER)
                .build()
        );

        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL,true);

    }

}