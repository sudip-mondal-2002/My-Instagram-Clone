package com.sudip.myinstagram;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.parse.LogInCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class MainActivity extends AppCompatActivity {
    Boolean signUpModeActive = true;
    Button signUpButton;
    TextView signUpChangerText;
    EditText usernameEditText;
    EditText passwordEditText;
    ImageView logoImage;
    ConstraintLayout background;
    public void changeMode(View view) {
        signUpModeActive = !signUpModeActive;
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signUpModeActive){
                    signUp(v);
                }else{
                    login(v);
                }

            }
        });
        if (signUpModeActive){
            signUpChangerText.setText("Already have an account? Log In");
            signUpButton.setText("SIGN UP");
        }else{
            signUpChangerText.setText("Don't have an account? Sign Up");
            signUpButton.setText("LOG IN");
        }
    }
    private void login(View view){
        ParseUser.logOut();
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (user==null){
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "User logged in " + user.getUsername(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(),UsersListActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
    private void signUp(View view) {
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (username.equals("") || password.equals("")) {
            Toast.makeText(this, "A username and password is required", Toast.LENGTH_LONG).show();
        } else {
            ParseUser user = new ParseUser();
            user.setUsername(username);
            user.setPassword(password);
            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        Toast.makeText(MainActivity.this, "Sign Up Successful", Toast.LENGTH_SHORT).show();
                        login(view);
                    } else {
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void hideKeyboard(){
        View view = getCurrentFocus();
        if (view !=null){
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),0);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ParseUser user = ParseUser.getCurrentUser();
        if (user==null){
            signUpButton = (Button) findViewById(R.id.signup);
            signUpChangerText = (TextView) findViewById(R.id.toggler);
            signUpButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (signUpModeActive){
                        signUp(v);
                    }else{
                        login(v);
                    }

                }
            });
            if (signUpModeActive){
                signUpChangerText.setText("Already have an account? Log In");
                signUpButton.setText("SIGN UP");
            }else{
                signUpChangerText.setText("Don't have an account? Sign Up");
                signUpButton.setText("LOG IN");
            }

            usernameEditText = (EditText) findViewById(R.id.userName);
            passwordEditText = (EditText) findViewById(R.id.Password);

            passwordEditText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode==KeyEvent.KEYCODE_ENTER && event.getAction()==KeyEvent.ACTION_DOWN){
                        signUp(v);
                    }
                    return false;
                }
            });

            logoImage=(ImageView) findViewById(R.id.logoimage);
            background = (ConstraintLayout) findViewById(R.id.background);
            logoImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyboard();
                }
            });
            background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyboard();
                }
            });
        }else{
            Intent intent = new Intent(getApplicationContext(),UsersListActivity.class);
            startActivity(intent);
            finish();
        }

        ParseAnalytics.trackAppOpenedInBackground(getIntent());
    }
}